package co.wellsen.maorikeyboard;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;
import java.util.List;

/**
 * Project MaoriKeyboard.
 *
 * Created by wellsen on 20/02/18.
 */

public class CustomKeyboardView extends KeyboardView {

  public CustomKeyboardView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public CustomKeyboardView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  protected boolean onLongPress(Keyboard.Key popupKey) {
    switch (popupKey.codes[0]) {
      case 'a':
        sendKey('ā');
        return true;

      case 'i':
        sendKey('ī');
        return true;

      case 'u':
        sendKey('ū');
        return true;

      case 'e':
        sendKey('ē');
        return true;

      case 'o':
        sendKey('ō');
        return true;

      case 'A':
        sendKey('Ā');
        return true;

      case 'I':
        sendKey('Ī');
        return true;

      case 'U':
        sendKey('Ū');
        return true;

      case 'E':
        sendKey('Ē');
        return true;

      case 'O':
        sendKey('Ō');
        return true;

      default:
        return super.onLongPress(popupKey);
    }
  }

  private void sendKey(int key) {
    getOnKeyboardActionListener().onKey(key, null);
  }

  @Override
  public void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    List<Keyboard.Key> keys = getKeyboard().getKeys();
    for (Keyboard.Key key : keys) {
      Drawable dr = null;
      if (key.codes[0] == -10004) {
        switch (((MaoriImeService) getContext()).getMacronState()) {
          case MaoriImeService.MACRON_OFF:
          case MaoriImeService.MACRON_ON:
            dr = (Drawable) getContext().getResources().getDrawable(R.drawable.background_key_gray);

            break;
          case MaoriImeService.MACRON_CAPS:
            dr = (Drawable) getContext().getResources().getDrawable(R.drawable.background_key_blue);

            break;
        }

        dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
        dr.draw(canvas);

        if (key.icon != null) {
          dr = key.icon;
          dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
          dr.draw(canvas);
        }
      } else if (key.codes[0] == 10 || key.codes[0] == -5 || key.codes[0] == -10001) {
        dr = getResources().getDrawable(R.drawable.background_key_gray);

        dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
        dr.draw(canvas);

        if (key.icon != null) {
          dr = key.icon;
          dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
          dr.draw(canvas);
        }
      } else if (key.codes[0] == -10002) {
        switch (((MaoriImeService) getContext()).getMaoriShiftState()) {
          case MaoriImeService.MAORI_SHIFT_OFF:
          case MaoriImeService.MAORI_SHIFT_ON:
            dr = getResources().getDrawable(R.drawable.background_key_gray);

            dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
            dr.draw(canvas);

            if (key.icon != null) {
              dr = key.icon;
              dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
              dr.draw(canvas);
            }

            break;
          case MaoriImeService.MAORI_SHIFT_CAPS:
            dr = getResources().getDrawable(R.drawable.background_key_blue);

            dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
            dr.draw(canvas);

            if (key.icon != null) {
              dr = key.icon;
              dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
              dr.draw(canvas);
            }

            break;
        }
      } else if (key.codes[0] == -10003) {
        switch (((MaoriImeService) getContext()).getEnglishState()) {
          case MaoriImeService.ENGLISH_OFF:
            dr = getResources().getDrawable(R.drawable.background_key_gray);
            dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
            dr.draw(canvas);

            dr = getResources().getDrawable(R.drawable.sym_keyboard_shift);
            dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
            dr.draw(canvas);

            break;
          case MaoriImeService.ENGLISH_ON:
            dr = getResources().getDrawable(R.drawable.background_key_gray);
            dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
            dr.draw(canvas);

            dr = getResources().getDrawable(R.drawable.sym_keyboard_shift_on);
            dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
            dr.draw(canvas);

            break;
          case MaoriImeService.ENGLISH_CAPS:
            dr = getResources().getDrawable(R.drawable.background_key_blue);

            dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
            dr.draw(canvas);

            dr = getResources().getDrawable(R.drawable.sym_keyboard_shift_caps);
            dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
            dr.draw(canvas);

            break;
        }
      } else {
        if (key.pressed) {
          dr = getResources().getDrawable(R.drawable.background_key_pressed);
        } else {
          dr = getResources().getDrawable(R.drawable.background_key_white);
        }

        dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
        dr.draw(canvas);
      }
    }

    Paint mPaint = new Paint();
    mPaint.setAntiAlias(true);
    mPaint.setTextSize(getResources().getDimensionPixelSize(R.dimen.keyTextSize));
    mPaint.setTextAlign(Paint.Align.CENTER);
    mPaint.setAlpha(255);

    Paint paint = mPaint;

    final int kbdPaddingLeft = getPaddingLeft();
    final int kbdPaddingTop = getPaddingTop();

    final int mLabelTextSize = getResources().getDimensionPixelSize(R.dimen.keyTextSize);
    final int mKeyTextSize = getResources().getDimensionPixelSize(R.dimen.keyTextSize);

    final Drawable keyBackground = getBackground();

    final int keyCount = getKeyboard().getKeys().size();
    for (int i = 0; i < keyCount; i++) {
      final Keyboard.Key key = getKeyboard().getKeys().get(i);

      if (key.label != null) {

        int[] drawableState = key.getCurrentDrawableState();
        keyBackground.setState(drawableState);

        // Switch the character to uppercase if shift is pressed
        String label = key.label == null? null : adjustCase(key.label).toString();

        canvas.translate(key.x + kbdPaddingLeft, key.y + kbdPaddingTop);

        Drawable dr = null;
        if (label != null) {
          if (true || label.length() > 1 && key.codes.length < 2) {
            if (label.equalsIgnoreCase("Māori")) {
              paint.setTextSize(getResources().getDimensionPixelSize(R.dimen.keyTextSizeForTextMaori));
            } else {
              paint.setTextSize(mKeyTextSize);
            }

            paint.setColor(Color.WHITE);
            //paint.setTypeface(Typeface.DEFAULT_BOLD);
            canvas.drawText(label,
                (key.width - getPaddingLeft() - getPaddingRight()) / 2
                    + getPaddingLeft(),
                (key.height - getPaddingTop() - getPaddingBottom()) / 2
                    + (paint.getTextSize() - paint.descent()) / 2 + getPaddingTop(),
                paint);

            if (label.equalsIgnoreCase("Māori")) {
              paint.setTextSize(getResources().getDimensionPixelSize(R.dimen.keyTextSizeForTextMaori));
              paint.setColor(0xFFB7B7B7);
            } else {
              paint.setTextSize(mKeyTextSize);
              paint.setColor(Color.BLACK);
            }

            paint.setTypeface(Typeface.DEFAULT);
            canvas.drawText(label,
                (key.width - getPaddingLeft() - getPaddingRight()) / 2
                    + getPaddingLeft(),
                (key.height - getPaddingTop() - getPaddingBottom()) / 2
                    + (paint.getTextSize() - paint.descent()) / 2 + getPaddingTop(),
                paint);
          } else {

          }

        } else if (key.icon != null) {

        }
        canvas.translate(-key.x - kbdPaddingLeft, -key.y - kbdPaddingTop);
      }
    }
  }

  private CharSequence adjustCase(CharSequence label) {
    if (getKeyboard().isShifted() && label != null && label.length() < 3
        && Character.isLowerCase(label.charAt(0))) {
      label = label.toString().toUpperCase();
    }
    return label;
  }
}
