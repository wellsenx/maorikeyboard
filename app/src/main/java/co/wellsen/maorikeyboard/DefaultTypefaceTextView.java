package co.wellsen.maorikeyboard;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.TextView;

/**
 * Created by user on 3/8/18.
 */

public class DefaultTypefaceTextView extends TextView {

  public DefaultTypefaceTextView(Context context) {
    super(context);

    init();
  }

  public DefaultTypefaceTextView(Context context, AttributeSet attrs) {
    super(context, attrs);

    init();
  }

  public DefaultTypefaceTextView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    init();
  }

  private void init() {
    int maxWidth = 0;

    WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
    Display display = windowManager.getDefaultDisplay();

    maxWidth = (int)(((double) 15 * display.getWidth()) / 100);

    setWidth(maxWidth);
  }
  //--

  @Override
  public void setTypeface(Typeface tf) {
    super.setTypeface(Typeface.DEFAULT);
  }
}
