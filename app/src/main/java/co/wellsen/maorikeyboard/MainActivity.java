package co.wellsen.maorikeyboard;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);

    setContentView(R.layout.activity_main);

    Button btnActivateKeyboard = findViewById(R.id.btn_activate_keyboard);
    btnActivateKeyboard.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        try {

          startActivity(new Intent(Settings.ACTION_INPUT_METHOD_SETTINGS));
        } catch (ActivityNotFoundException e) {
          e.printStackTrace();
        }
      }
    });

    Button btnChangeKeyboard = findViewById(R.id.btn_change_keyboard);
    btnChangeKeyboard.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        InputMethodManager imm =
            (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
          imm.showInputMethodPicker();
        }
      }
    });

    TextView tvVersion = findViewById(R.id.tv_version);
    tvVersion.setText(
        String.format("%s\nVersion %s", getString(R.string.app_name), BuildConfig.VERSION_NAME));
  }

}
