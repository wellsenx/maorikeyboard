package co.wellsen.maorikeyboard;

import android.annotation.SuppressLint;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.media.AudioManager;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

/**
 * Project MaoriKeyboard.
 *
 * Created by wellsen on 15/02/18.
 */

public class MaoriImeService extends InputMethodService
    implements CustomKeyboardView.OnKeyboardActionListener {

  public static final int MACRON_OFF = 0;
  public static final int MACRON_ON = 1;
  public static final int MACRON_CAPS = 2;
  public static final int ENGLISH_OFF = 0;
  public static final int ENGLISH_ON = 1;
  public static final int ENGLISH_CAPS = 2;
  private static final int LANGUAGE_MAORI = 0;
  private static final int LANGUAGE_SYMBOL = 1;
  private static final int LANGUAGE_ENGLISH = 2;
  public static final int MAORI_SHIFT_OFF = 0;
  public static final int MAORI_SHIFT_ON = 1;
  public static final int MAORI_SHIFT_CAPS = 2;
  private static final int SYMBOL_PAGE_1 = 0;
  private static final int SYMBOL_PAGE_2 = 1;

  private static final int KEYCODE_A = -100001;
  private static final int KEYCODE_I = -100002;
  private static final int KEYCODE_U = -100003;
  private static final int KEYCODE_E = -100004;
  private static final int KEYCODE_O = -100005;
  private static final int KEYCODE_WH = -200001;
  private static final int KEYCODE_NG = -200002;

  private static final int KEYCODE_LANGUAGE_CHANGE = -10001;
  private static final int KEYCODE_MAORI_SHIFT_CHANGE = -10002;
  private static final int KEYCODE_ENGLISH_SHIFT_CHANGE = -10003;
  private static final int KEYCODE_MACRON_CHANGE = -10004;
  private static final int KEYCODE_SYMBOL_PAGE_CHANGE = -20001;

  private int englishState = ENGLISH_OFF;

  private int languageState = LANGUAGE_MAORI;
  private int maoriShiftState = MAORI_SHIFT_OFF;
  //private boolean englishShift = false;
  private int macronState = MACRON_OFF;
  private int symbolPageState = SYMBOL_PAGE_1;

  private CustomKeyboardView kv;
  private Keyboard keyboard;

  private int options;

  private String mWordSeparators;

  @Override
  public void onCreate() {
    super.onCreate();

    mWordSeparators = getResources().getString(R.string.word_separators);
  }

  @SuppressLint("InflateParams")
  @Override
  public View onCreateInputView() {
    kv = (CustomKeyboardView) getLayoutInflater().inflate(R.layout.keyboard, null);
    kv.setPreviewEnabled(false);
    keyboard = new Keyboard(this, R.xml.maori);
    kv.setKeyboard(keyboard);
    kv.setOnKeyboardActionListener(this);
    kv.setLongClickable(true);
    return kv;
  }

  private void playClick(int keyCode) {
    AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
    if (am == null) {
      return;
    }
    float vol = 1.0f; //This will be 0.8x of the default system sound
    switch (keyCode) {
      case 32:
        am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR, vol);
        break;
      case Keyboard.KEYCODE_DONE:
      case 10:
        am.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN, vol);
        break;
      case Keyboard.KEYCODE_DELETE:
        am.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE, vol);
        break;
      default:
        am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD, vol);
        break;
    }
  }

  private void vibrate() {
    if (getSystemService(VIBRATOR_SERVICE) == null) {
      return;
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(
          VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));
    } else {
      ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(50);
    }
  }

  @Override
  public void onKey(int primaryCode, int[] keyCodes) {
    InputConnection ic = getCurrentInputConnection();
    playClick(primaryCode);
    //vibrate();
    char code;
    switch (primaryCode) {
      case KEYCODE_LANGUAGE_CHANGE:
        changeLanguage();
        break;

      case KEYCODE_MAORI_SHIFT_CHANGE:
        changeMaoriShiftMode();
        break;

      case KEYCODE_ENGLISH_SHIFT_CHANGE:
        changeLatinShiftMode();
        break;

      case KEYCODE_MACRON_CHANGE:
        changeMacron();
        break;

      case KEYCODE_SYMBOL_PAGE_CHANGE:
        changeSymbolPage();
        break;

      case KEYCODE_WH:
        switch (maoriShiftState) {
          case MAORI_SHIFT_OFF:
            ic.commitText(String.valueOf("wh"), 1);
            break;
          case MAORI_SHIFT_ON:
            ic.commitText(String.valueOf("Wh"), 1);
            break;
          case MAORI_SHIFT_CAPS:
            ic.commitText(String.valueOf("WH"), 1);
            break;
        }

        if (macronState == MACRON_ON) {
          macronState = -1;

          changeMacron();
        }

        if (maoriShiftState == MAORI_SHIFT_ON) {
          maoriShiftState = -1;

          changeMaoriShiftMode();
        }
        break;
      case KEYCODE_NG:
        switch (maoriShiftState) {
          case MAORI_SHIFT_OFF:
            ic.commitText(String.valueOf("ng"), 1);
            break;
          case MAORI_SHIFT_ON:
            ic.commitText(String.valueOf("Ng"), 1);
            break;
          case MAORI_SHIFT_CAPS:
            ic.commitText(String.valueOf("NG"), 1);
            break;
        }

        if (macronState == MACRON_ON) {
          macronState = -1;

          changeMacron();
        }

        if (maoriShiftState == MAORI_SHIFT_ON) {
          maoriShiftState = -1;

          changeMaoriShiftMode();
        }
        break;
      case Keyboard.KEYCODE_DELETE:
        ic.deleteSurroundingText(1, 0);
        break;
      case Keyboard.KEYCODE_SHIFT:
        //caps = !caps;
        //keyboard.setShifted(caps);
        kv.invalidateAllKeys();
        break;
      case Keyboard.KEYCODE_DONE:
        ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
        break;
      default:
        if (isWordSeparator(primaryCode)) {
          // Handle separator
          //if (mComposing.length() > 0) {
            //commitTyped(getCurrentInputConnection());
          //}
          sendKey(primaryCode);
          //updateShiftKeyState(getCurrentInputEditorInfo());

          return;
        }

        code = (char) primaryCode;
        if (Character.isLetter(code) && (englishState == ENGLISH_ON || englishState == ENGLISH_CAPS)) {
          code = Character.toUpperCase(code);
        }
        ic.commitText(String.valueOf(code), 1);

        if (englishState == ENGLISH_ON) {
          englishState = -1;

          changeLatinShiftMode();
        }

        if (macronState == MACRON_ON) {
          macronState = -1;

          changeMacron();
        }

        if (maoriShiftState == MAORI_SHIFT_ON) {
          maoriShiftState = -1;

          changeMaoriShiftMode();
        }
    }
  }

  @Override
  public void onPress(int primaryCode) {
    switch (primaryCode) {
      case -10001:
      case -10002:
      case -10003:
      case -10004:
      case -20001:
      case -5:
      case 10:
        return;
      default:
        kv.setPreviewEnabled(true);
    }

    /*
    int upperKeyCode[] = {
        48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 101, 114, 116, 105, 111, 112, 69, 82, 84, 73, 79,
        80, 113, 119, 121, 117, 81, 87
    };
    if (contains(upperKeyCode, primaryCode)) {
      kv.setPopupOffset(0, 300);
    } else {
      kv.setPopupOffset(0, 0);
    }
    */
  }

  @Override
  public void onRelease(int primaryCode) {
    kv.setPreviewEnabled(false);
  }

  /**
   * Use this to monitor key events being delivered to the application.
   * We get first crack at them, and can either resume them or let them
   * continue to the app.
   */
  @Override public boolean onKeyDown(int keyCode, KeyEvent event) {
    switch (keyCode) {
      case KeyEvent.KEYCODE_ENTER:
        // Let the underlying text editor always handle these.
        return false;

      default:
    }

    return super.onKeyDown(keyCode, event);
  }


  @Override
  public void onText(CharSequence text) {

  }

  @Override
  public void swipeDown() {

  }

  @Override
  public void swipeLeft() {

  }

  @Override
  public void swipeRight() {

  }

  @Override
  public void swipeUp() {

  }

  @Override
  public void onStartInputView(EditorInfo info, boolean restarting) {
    super.onStartInputView(info, restarting);

    if (SharedPreferencesHelper.getLayout(this) != 0) {
      keyboard = new Keyboard(this, SharedPreferencesHelper.getLayout(this));
      kv.setKeyboard(keyboard);
    } else {
      languageState = SharedPreferencesHelper.getLanguageState(this);
      macronState = SharedPreferencesHelper.getMacronState(this);
      switch (languageState) {
        case LANGUAGE_MAORI:
          keyboard = new Keyboard(this, R.xml.maori);
          SharedPreferencesHelper.saveLayout(this, R.xml.maori);
          break;

        case LANGUAGE_SYMBOL:
          keyboard = new Keyboard(this, R.xml.symbol_page_1);
          SharedPreferencesHelper.saveLayout(this, R.xml.symbol_page_1);
          break;

        case LANGUAGE_ENGLISH:
          keyboard = new Keyboard(this, R.xml.english);
          SharedPreferencesHelper.saveLayout(this, R.xml.english);
          break;

        default:
          languageState = LANGUAGE_MAORI;
          keyboard = new Keyboard(this, R.xml.maori);
          SharedPreferencesHelper.saveLayout(this, R.xml.maori);
          break;
      }

      resetState();
      kv.setKeyboard(keyboard);
    }

    options = info.imeOptions;
    setEnterKey();
  }

  @Override
  public void onWindowHidden() {
    super.onWindowHidden();
    resetState();

    SharedPreferencesHelper.saveLayout(this, 0);
    SharedPreferencesHelper.saveLanguageState(this, languageState);
    SharedPreferencesHelper.saveMacronState(this, macronState);
  }

  public boolean contains(final int[] array, final int key) {
    for (final int i : array) {
      if (i == key) {
        return true;
      }
    }
    return false;
  }

  private void setEnterKey() {
    Keyboard.Key mEnterKey = null;
    for (Keyboard.Key key : keyboard.getKeys()) {
      if (key.codes[0] == 10) {
        mEnterKey = key;

        switch (options&(EditorInfo.IME_MASK_ACTION|EditorInfo.IME_FLAG_NO_ENTER_ACTION)) {
          case EditorInfo.IME_ACTION_GO:
            mEnterKey.iconPreview = null;
            mEnterKey.icon = null;
            mEnterKey.label = getResources().getText(R.string.label_go_key);
            break;
          case EditorInfo.IME_ACTION_NEXT:
            mEnterKey.iconPreview = null;
            mEnterKey.icon = null;
            mEnterKey.label = getResources().getText(R.string.label_next_key);
            break;
          case EditorInfo.IME_ACTION_SEARCH:
            mEnterKey.icon = getResources().getDrawable(R.drawable.sym_keyboard_search);
            mEnterKey.label = null;
            break;
          case EditorInfo.IME_ACTION_SEND:
            mEnterKey.iconPreview = null;
            mEnterKey.icon = null;
            mEnterKey.label = getResources().getText(R.string.label_send_key);
            break;
          default:
            mEnterKey.icon = getResources().getDrawable(R.drawable.sym_keyboard_return);
            mEnterKey.label = null;
            break;
        }

        kv.invalidateAllKeys();
        break;
      }
    }
  }

  private void changeLanguage() {
    ++languageState;
    switch (languageState) {
      case LANGUAGE_MAORI:
        keyboard = new Keyboard(this, R.xml.maori);
        SharedPreferencesHelper.saveLayout(this, R.xml.maori);
        break;

      case LANGUAGE_SYMBOL:
        keyboard = new Keyboard(this, R.xml.symbol_page_1);
        SharedPreferencesHelper.saveLayout(this, R.xml.symbol_page_1);
        break;

      case LANGUAGE_ENGLISH:
        keyboard = new Keyboard(this, R.xml.english);
        SharedPreferencesHelper.saveLayout(this, R.xml.english);
        break;

      default:
        languageState = LANGUAGE_MAORI;
        keyboard = new Keyboard(this, R.xml.maori);
        SharedPreferencesHelper.saveLayout(this, R.xml.maori);
        break;
    }

    SharedPreferencesHelper.saveLanguageState(this, languageState);
    resetState();

    kv.setKeyboard(keyboard);
    setEnterKey();
  }

  private void resetState() {
    maoriShiftState = MAORI_SHIFT_OFF;
    //englishShift = false;
    englishState = ENGLISH_OFF;
    symbolPageState = SYMBOL_PAGE_1;
    macronState = MACRON_OFF;
  }

  private void changeMaoriShiftMode() {
    ++maoriShiftState;
    switch (maoriShiftState) {
      case MAORI_SHIFT_OFF:
        switch (macronState) {
          case MACRON_OFF:
            keyboard = new Keyboard(this, R.xml.maori);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori);
            break;

          case MACRON_ON:
            keyboard = new Keyboard(this, R.xml.maori_macron_on);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_macron_on);
            break;

          case MACRON_CAPS:
            keyboard = new Keyboard(this, R.xml.maori_macron_caps);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_macron_caps);
            break;

          default:
            keyboard = new Keyboard(this, R.xml.maori);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori);
            break;
        }
        break;

      case MAORI_SHIFT_ON:
        switch (macronState) {
          case MACRON_OFF:
            keyboard = new Keyboard(this, R.xml.maori_shifted);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_shifted);
            break;

          case MACRON_ON:
            keyboard = new Keyboard(this, R.xml.maori_shifted_macron_on);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_shifted_macron_on);
            break;

          case MACRON_CAPS:
            keyboard = new Keyboard(this, R.xml.maori_shifted_macron_caps);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_shifted_macron_caps);
            break;

          default:
            keyboard = new Keyboard(this, R.xml.maori_shifted);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_shifted);
            break;
        }
        break;

      case MAORI_SHIFT_CAPS:
        switch (macronState) {
          case MACRON_OFF:
            keyboard = new Keyboard(this, R.xml.maori_caps);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_caps);
            break;

          case MACRON_ON:
            keyboard = new Keyboard(this, R.xml.maori_caps_macron_on);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_caps_macron_on);
            break;

          case MACRON_CAPS:
            keyboard = new Keyboard(this, R.xml.maori_caps_macron_caps);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_caps_macron_caps);
            break;

          default:
            keyboard = new Keyboard(this, R.xml.maori_caps);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_caps);
            break;
        }
        break;

      default:
        maoriShiftState = MAORI_SHIFT_OFF;
        switch (macronState) {
          case MACRON_OFF:
            keyboard = new Keyboard(this, R.xml.maori);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori);
            break;

          case MACRON_ON:
            keyboard = new Keyboard(this, R.xml.maori_macron_on);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_macron_on);
            break;

          case MACRON_CAPS:
            keyboard = new Keyboard(this, R.xml.maori_macron_caps);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_macron_caps);
            break;

          default:
            keyboard = new Keyboard(this, R.xml.maori);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori);
            break;
        }
        break;
    }

    kv.setKeyboard(keyboard);
    setEnterKey();
  }

  private void changeLatinShiftMode() {
    ++englishState;
    if (englishState == ENGLISH_ON || englishState == ENGLISH_CAPS) {
      keyboard.setShifted(true);
    } else {
      keyboard.setShifted(false);
      englishState = ENGLISH_OFF;
    }
    kv.invalidateAllKeys();

    /*
    englishShift = !englishShift;
    keyboard.setShifted(englishShift);
    kv.invalidateAllKeys();
    */

    //keyboard = new Keyboard(this, englishShift ? R.xml.english_caps : R.xml.english);
    //kv.setKeyboard(keyboard);
  }

  private void changeSymbolPage() {
    ++symbolPageState;
    switch (symbolPageState) {
      case SYMBOL_PAGE_1:
        keyboard = new Keyboard(this, R.xml.symbol_page_1);
        SharedPreferencesHelper.saveLayout(this, R.xml.symbol_page_1);
        break;

      case SYMBOL_PAGE_2:
        keyboard = new Keyboard(this, R.xml.symbol_page_2);
        SharedPreferencesHelper.saveLayout(this, R.xml.symbol_page_2);
        break;

      default:
        symbolPageState = SYMBOL_PAGE_1;
        keyboard = new Keyboard(this, R.xml.symbol_page_1);
        SharedPreferencesHelper.saveLayout(this, R.xml.symbol_page_1);
        break;
    }

    kv.setKeyboard(keyboard);
    setEnterKey();
  }

  private void changeMacron() {
    ++macronState;
    switch (macronState) {
      case MACRON_OFF:
        switch (maoriShiftState) {
          case MAORI_SHIFT_OFF:
            keyboard = new Keyboard(this, R.xml.maori);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori);
            break;

          case MAORI_SHIFT_ON:
            keyboard = new Keyboard(this, R.xml.maori_shifted);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_shifted);
            break;

          case MAORI_SHIFT_CAPS:
            keyboard = new Keyboard(this, R.xml.maori_caps);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_caps);
            break;

          default:
            keyboard = new Keyboard(this, R.xml.maori);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori);
            break;
        }
        break;

      case MACRON_ON:
        switch (maoriShiftState) {
          case MAORI_SHIFT_OFF:
            keyboard = new Keyboard(this, R.xml.maori_macron_on);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_macron_on);
            break;

          case MAORI_SHIFT_ON:
            keyboard = new Keyboard(this, R.xml.maori_shifted_macron_on);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_shifted_macron_on);
            break;

          case MAORI_SHIFT_CAPS:
            keyboard = new Keyboard(this, R.xml.maori_caps_macron_on);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_caps_macron_on);
            break;

          default:
            keyboard = new Keyboard(this, R.xml.maori_macron_on);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_macron_on);
            break;
        }
        break;

      case MACRON_CAPS:
        switch (maoriShiftState) {
          case MAORI_SHIFT_OFF:
            keyboard = new Keyboard(this, R.xml.maori_macron_caps);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_macron_caps);
            break;

          case MAORI_SHIFT_ON:
            keyboard = new Keyboard(this, R.xml.maori_shifted_macron_caps);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_shifted_macron_caps);
            break;

          case MAORI_SHIFT_CAPS:
            keyboard = new Keyboard(this, R.xml.maori_caps_macron_caps);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_caps_macron_caps);
            break;

          default:
            keyboard = new Keyboard(this, R.xml.maori_macron_caps);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_macron_caps);
            break;
        }
        break;

      default:
        macronState = MACRON_OFF;
        switch (maoriShiftState) {
          case MAORI_SHIFT_OFF:
            keyboard = new Keyboard(this, R.xml.maori);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori);
            break;

          case MAORI_SHIFT_ON:
            keyboard = new Keyboard(this, R.xml.maori_shifted);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_shifted);
            break;

          case MAORI_SHIFT_CAPS:
            keyboard = new Keyboard(this, R.xml.maori_caps);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori_caps);
            break;

          default:
            keyboard = new Keyboard(this, R.xml.maori);
            SharedPreferencesHelper.saveLayout(this, R.xml.maori);
            break;
        }
        break;
    }

    SharedPreferencesHelper.saveMacronState(this, macronState);

    kv.setKeyboard(keyboard);
    setEnterKey();
  }

  public boolean isWordSeparator(int code) {
    String separators = getWordSeparators();
    return separators.contains(String.valueOf((char)code));
  }

  private String getWordSeparators() {
    return mWordSeparators;
  }

  /**
   * Helper to send a character to the editor as raw key events.
   */
  private void sendKey(int keyCode) {
    switch (keyCode) {
      case '\n':
        keyDownUp(KeyEvent.KEYCODE_ENTER);
        break;
      default:
        if (keyCode >= '0' && keyCode <= '9') {
          keyDownUp(keyCode - '0' + KeyEvent.KEYCODE_0);
        } else {
          getCurrentInputConnection().commitText(String.valueOf((char) keyCode), 1);
        }
        break;
    }
  }

  /**
   * Helper to send a key down / key up pair to the current editor.
   */
  private void keyDownUp(int keyEventCode) {
    getCurrentInputConnection().sendKeyEvent(
        new KeyEvent(KeyEvent.ACTION_DOWN, keyEventCode));
    getCurrentInputConnection().sendKeyEvent(
        new KeyEvent(KeyEvent.ACTION_UP, keyEventCode));
  }

  public int getEnglishState() {
    return englishState;
  }

  public int getMacronState() {
    return macronState;
  }

  public int getMaoriShiftState() {
    return maoriShiftState;
  }
}
