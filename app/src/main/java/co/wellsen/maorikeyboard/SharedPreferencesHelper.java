package co.wellsen.maorikeyboard;

import android.content.Context;

/**
 * Created by user on 2/22/18.
 */

public class SharedPreferencesHelper {

  private static final String PREF_NAME = "app_db";
  private static final String KEY_LAYOUT = "layout";
  private static final String KEY_LANGUAGE = "language";
  private static final String KEY_MACRON = "macron";

  public static void saveLayout(Context context, int layout) {
    context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        .edit()
        .putInt(KEY_LAYOUT, layout)
        .commit();
  }

  public static int getLayout(Context context) {
    return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        .getInt(KEY_LAYOUT, 0);
  }

  public static void saveLanguageState(Context context, int language) {
    context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        .edit()
        .putInt(KEY_LANGUAGE, language)
        .commit();
  }

  public static int getLanguageState(Context context) {
    return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        .getInt(KEY_LANGUAGE, 0);
  }

  public static void saveMacronState(Context context, int macron) {
    context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        .edit()
        .putInt(KEY_MACRON, macron)
        .commit();
  }

  public static int getMacronState(Context context) {
    return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
          .getInt(KEY_MACRON, 0);
  }
}
